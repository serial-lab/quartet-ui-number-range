"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

// Copyright (c) 2018 SerialLab Corp.
//
// GNU GENERAL PUBLIC LICENSE
//    Version 3, 29 June 2007
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
const React = qu4rtet.require("react");
const { Component } = React;
const { pluginRegistry } = qu4rtet.require("./plugins/pluginRegistration");
const { SingleCardPicker } = qu4rtet.require("./components/elements/SingleCardPicker");
const { Dialog, Button, Tag } = qu4rtet.require("@blueprintjs/core");
const classNames = qu4rtet.require("classnames");

const TemplateEntry = props => {
  const updateFieldVal = entry => {
    // trigger a redux form field value change
    props.changeValue(entry);
  };
  return React.createElement(
    "div",
    { key: props.entry.id, onClick: updateFieldVal.bind(undefined, props.entry) },
    React.createElement(
      "h5",
      null,
      props.entry.name
    ),
    React.createElement("div", { style: { width: "260px", height: "130px", background: "#CCC" } }),
    React.createElement(
      "ul",
      { className: "picker-data-list" },
      props.entry.name ? React.createElement(
        "li",
        null,
        props.entry.name
      ) : null,
      props.entry.urn ? React.createElement(
        "li",
        null,
        props.entry.urn
      ) : null
    )
  );
};

class TemplateDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      templateName: null,
      templateContent: null,
      edited: false // to preserve overridden template if changed from the form.
    };
  }

  componentDidMount() {
    this.setState({
      templateName: null,
      templateContent: null,
      edited: false // to preserve overridden template if changed from the form.
    });
    this.setTemplateName(this.props);
  }

  /*componentWillReceiveProps(nextProps) {
    this.setTemplateName(nextProps);
  }*/

  setTemplateName(props) {
    if (this.state.edited === false && props.existingValues && props.existingValues.template) {
      const template = props.existingValues.template;
      if (typeof template === "object") {
        this.setState({
          templateName: template.name,
          templateContent: template.content
        });
      } else {
        pluginRegistry.getServer(props.server).fetchObject("templates_templates_read", {
          id: template
        }).then(item => {
          this.setState({
            templateName: item.name,
            templateContent: item.content
          });
        });
      }
    }
  }

  changeValue(entry) {
    this.setState({ templateName: entry.name, templateContent: entry.content, edited: true }, () => {
      this.props.changeFieldValue(this.props.formName, "template", entry.id);
      this.props.toggleTemplateDialog();
    });
  }

  render() {
    // modify props to use the proper next and count.
    let props = _extends({}, this.props);
    props.next = props.templatesNext;
    props.count = props.templatesCount;
    return React.createElement(
      "div",
      null,
      React.createElement(
        "div",
        null,
        this.state.templateName ? React.createElement(
          "div",
          null,
          React.createElement(
            Tag,
            {
              style: { cursor: "pointer" },
              className: "pt-intent-primary",
              onClick: this.props.toggleTemplateDialog },
            this.state.templateName
          ),
          React.createElement(
            "pre",
            {
              style: {
                whiteSpace: "pre-wrap",
                width: "auto",
                height: "250px",
                overflow: "auto"
              } },
            this.state.templateContent
          )
        ) : React.createElement(Button, {
          onClick: this.props.toggleTemplateDialog,
          text: "Select Template"
        })
      ),
      React.createElement(
        Dialog,
        {
          iconName: "inbox",
          isOpen: this.props.isTemplateOpen,
          onClose: this.props.toggleTemplateDialog,
          style: { width: "80%" },
          className: classNames({
            "pt-dark": this.props.theme.includes("dark"),
            "wide-dialog": true
          }),
          title: "Select a Template" },
        React.createElement(
          "div",
          { className: "pt-dialog-body" },
          React.createElement(SingleCardPicker, _extends({}, props, {
            changeValue: this.changeValue.bind(this),
            loadEntries: this.props.loadEntries,
            entries: this.props.templates,
            entryClass: TemplateEntry
          }))
        ),
        React.createElement("div", { className: "pt-dialog-footer" })
      )
    );
  }
}
exports.TemplateDialog = TemplateDialog;