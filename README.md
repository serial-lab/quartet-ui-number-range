![pipeline](https://gitlab.com/serial-lab/quartet-ui-number-range/badges/master/pipeline.svg) | ![coverage](https://gitlab.com/serial-lab/quartet-ui-number-range/badges/master/coverage.svg)

# QU4RTET Number Range Desktop Plugin

This plugin provides the UI elements necessary to interact with the QU4RTET backend Serialbox module.